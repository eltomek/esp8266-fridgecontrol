#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <FS.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ThingSpeak.h>
#include <DNSServer.h>
#include <WiFiManager.h>

struct Config {
  uint8_t mode;
  float_t temperature;
  float_t hysteresis;
  uint8_t interval;
};

const std::vector<String> Modes = {"OFF", "ON", "AUTO"};

#define ONE_WIRE_BUS 2
#define RELAY_PIN 4

const unsigned long myChannelNumber = 322038;
const char * myWriteAPIKey = "H4KOZAP9QMSL53D0";

//const unsigned long myChannelNumber = 718431;
//const char * myWriteAPIKey = "2NMAXPJ1ZJUPHTQ7";

const String configFile = "config.txt";

const float_t rangeTemp[]     = {1.0, 25.0};
const float_t rangeHyst[]     = {0.1, 2.0};
const float_t rangeInterval[] = {1, 60};

const uint8_t DefaultMode         = 2;
const float_t DefaultTemperature  = 13.0;
const float_t DefaultHysteresis   = 0.5;
const uint8_t DefaultInterval     = 1;

const uint32_t loopPeriod = 1000; // 1 sec
uint32_t startMillis, currentMillis;
uint16_t loopCnt = 0;

void toggle();
void handleRoot();
void loadConfiguration(const String, Config &);
void saveConfiguration(const String, const Config &);
void printFile(const String);
void handleRoot();
void toggle();

Config config;
WiFiClient client;

ESP8266WebServer server(80);

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

void setup() {
  Serial.begin(115200);
  while (!Serial) continue;
  
  loadConfiguration(configFile, config);
  printFile(configFile);

  digitalWrite(ONE_WIRE_BUS, LOW);
  pinMode(ONE_WIRE_BUS, OUTPUT);

  digitalWrite(RELAY_PIN, LOW);
  pinMode(RELAY_PIN, OUTPUT);

  sensors.begin();
  WiFiManager wifiManager;
  wifiManager.setConfigPortalTimeout(180);  // try setting up WiFi but ignore and run fridge control regardless of connection status
  wifiManager.autoConnect();
  
  ThingSpeak.begin(client);

  server.on("/", handleRoot);
  server.begin();

  Serial.printf("Web server started @ %s\n", WiFi.localIP().toString().c_str());
  
  startMillis = millis();

  ESP.wdtDisable();
}

void loop() {
  currentMillis = millis();
  server.handleClient();

  if (currentMillis - startMillis >= loopPeriod) {
    startMillis = currentMillis;

    if (!(loopCnt % (config.interval * 60))) {
      toggle();
    }
    loopCnt++;
    ESP.wdtFeed();
  }
}

void loadConfiguration(const String filename, Config &config) {
  Serial.println(F("Loading configuration..."));

  while (!SPIFFS.begin()) {
    delay(1000);
  }
  
  File file = SPIFFS.open(filename, "r");

  StaticJsonDocument<256> doc;
  
  DeserializationError error = deserializeJson(doc, file);
  
  if (error)
    Serial.println(F("Failed to read config file"));

  config.mode         = doc["mode"]         | DefaultMode;
  config.temperature  = doc["temperature"]  | DefaultTemperature;
  config.hysteresis   = doc["hysteresis"]   | DefaultHysteresis;
  config.interval     = doc["interval"]     | DefaultInterval;
  
  file.close();
  SPIFFS.end();
}

void saveConfiguration(const String filename, const Config &config) {
  Serial.println(F("Saving configuration..."));
  while (!SPIFFS.begin()) delay(1000);

  SPIFFS.remove(filename);   // Delete existing file, otherwise the configuration is appended to the file

  // Open file for writing
  File file = SPIFFS.open(filename, "w");
  if (!file) {
    Serial.println(F("Failed to create file"));
    return;
  }

  StaticJsonDocument<256> doc;

  // Set the values in the document
  doc["mode"]         = config.mode;
  doc["temperature"]  = config.temperature;
  doc["hysteresis"]   = config.hysteresis;
  doc["interval"]     = config.interval;

  if (serializeJson(doc, file) == 0) {
    Serial.println(F("Failed to write to file"));
  }

  file.close();

  SPIFFS.end();
}

void printFile(const String filename) {
  while (!SPIFFS.begin()) delay(1000);

  // Open file for reading
  File file = SPIFFS.open(filename, "r");
  if (!file) {
    Serial.println(F("Failed to read file"));
    return;
  }

  // Extract each characters by one by one
  while (file.available()) {
    Serial.print((char)file.read());
  }
  Serial.println();

  // Close the file
  file.close();
  SPIFFS.end();
}

void handleRoot() {
  String html;
  html  = "<!doctype html>\n";
  html += "<html lang='pl_PL'>\n";
  html += "<meta charset='utf-8'>\n";
  html += "<body>\n";

  boolean configUpdate = false;
  std::vector<String> statusUpdate;
  String statusThingspeak, statusHtml;

  for (uint8_t i = 0; i < server.args(); i++) {
    
    if (String(server.argName(i)) == "mode") {
      uint8_t reqMode =  server.arg(i).toInt();

      if (reqMode < Modes.size() && reqMode != config.mode) {
        config.mode = server.arg(i).toInt();
        statusUpdate.push_back("Mode: " + String(config.mode));
        configUpdate = true;
      }
    }

    if (String(server.argName(i)) == "temp") {
      float_t reqTemp = server.arg(i).toFloat();

      if (reqTemp >= rangeTemp[0] && reqTemp <= rangeTemp[1] && reqTemp != config.temperature) {
        config.temperature = reqTemp;
        statusUpdate.push_back("Temp: " + String(config.temperature));
        configUpdate = true;
      }
    }

    if (String(server.argName(i)) == "hyst") {
      float_t reqHysteresis = server.arg(i).toFloat();

      if (reqHysteresis >= rangeHyst[0] && reqHysteresis <= rangeHyst[1] && reqHysteresis != config.hysteresis) {
        config.hysteresis = reqHysteresis;
        statusUpdate.push_back("Hysteresis: " + String(config.hysteresis));
        configUpdate = true;
      }
    }

    if (String(server.argName(i)) == "interval") {
      uint8_t reqInterval = server.arg(i).toInt();

      if (reqInterval >= rangeInterval[0] && reqInterval <= rangeInterval[1] && reqInterval != config.interval) {
        config.interval = reqInterval;
        statusUpdate.push_back("Interval: " + String(config.interval));
        configUpdate = true;
      }
    }
  }

  if (configUpdate) {
    saveConfiguration(configFile, config);

    for (size_t i = 0; i < statusUpdate.size(); i++) {
      statusThingspeak += statusUpdate.at(i) + " ";
      statusHtml += "<p>" + statusUpdate.at(i) + "</p>\n";
    }
    
    ThingSpeak.setStatus(statusThingspeak);
    printFile(configFile);
    toggle();
  }

  sensors.requestTemperatures(); // Send the command to get temperatures
  html += statusHtml;
  html += "<form method='post' style='padding: 20px; box-sizing: border-box;border: 2px solid #ccc;border-radius: 4px;background-color: #f8f8f8;'>\n";
  for(std::size_t i = 0; i < Modes.size(); i++) {
    html += "<label><input type='radio' name='mode' value='" + String(i) + "' " + (i == config.mode ? "checked" : "") + "/> " + Modes.at(i) + "</label>&nbsp;\n";
  }
  html += "<br/>Temperatura: \n";
  html += "<input name='temp' value='" + String(config.temperature) + "' size=3/> (" + String(rangeTemp[0]) + " - " + String(rangeTemp[1]) + ")<br/>\n";
  html += "Histereza: <input name='hyst' value='" + String(config.hysteresis) + "' size=3/> (" + String(rangeHyst[0]) + " - " + String(rangeHyst[1]) + ")<br/>\n";
  html += "Interwal: <input name='interval' value='" + String(config.interval) + "' size=3/> (" + String(rangeInterval[0]) + " - " + String(rangeInterval[1]) + ")<br/>\n";
  html += "<input type='submit' value=OK></form>\n";
  html += "<p>Aktualna temp w lodowce: " + String(sensors.getTempCByIndex(0)) + "</p>\n";
  html += "<p>Stan chlodzenia: " + String(digitalRead(RELAY_PIN))+" </p>\n";
  html += "</body>\n";
  html += "</html>\n";

  server.send(200, "text/html", html);
}

void toggle() {
  sensors.requestTemperatures();
  float_t currentTemp = sensors.getTempCByIndex(0);

  Serial.println(currentTemp);

  switch (config.mode)
  {
  case 0:
    digitalWrite(RELAY_PIN, LOW);
    break;
  
  case 1:
    digitalWrite(RELAY_PIN, HIGH);
    break;

  case 2:
    if (currentTemp - config.hysteresis / 2 >= config.temperature) {
      digitalWrite(RELAY_PIN, HIGH);
    }
    else if (currentTemp + config.hysteresis / 2 <= config.temperature) {
      digitalWrite(RELAY_PIN, LOW);
    }
    break;
  }

  if (WiFi.status() == WL_CONNECTED) {
    ThingSpeak.setField(1, currentTemp);
    ThingSpeak.setField(2, digitalRead(RELAY_PIN));
    int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
    
    if (x == 200) {
      Serial.println(F("Channel update successful"));
    }
    else {
      Serial.println("Problem updating channel. HTTP error code " + String(x));
    }
  }
}